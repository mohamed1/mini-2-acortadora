from django.http import HttpResponse, Http404, HttpResponseRedirect
from .models import Contenido
from django.views.decorators.csrf import csrf_exempt
from django.template import loader

def obtener_contenido():
	contenido_lista = Contenido.objects.all()
	template = loader.get_template("acorta/formulario_url.html")
	contexto = {
		"contenido_lista": contenido_lista
	}
	return template.render(contexto)

def obtener_id():
	i = 1
	contenidos = Contenido.objects.all()
	for contenido in contenidos:
		if contenido.short == str(i):
			return contenido.short
		else:
			return i

def url_correcion(url, short):
	if not (url.startswith("http://") or  url.startswith("https://")):
		url = "http://" + url
	if short == "":
		id = obtener_id()
		short = f"localhost://8000/{id}"
	else:
		short = f"localhost://8000/{short}"
	return url, short


@csrf_exempt
def index(request):
	if request.method == "POST":
		url = request.POST['url']
		short = request.POST['short']
		url, short = url_correcion(url, short)
		try:
			contenido = Contenido.objects.get(url=url)
			contenido.short = short
			contenido.save()
			template = loader.get_template("acorta/formulario_url.html")
			html = f"{template}" \
				   f"<div>{obtener_contenido()}</div>"
			return HttpResponse(html)
		except Contenido.DoesNotExist:
			contenido = Contenido(url=url, short=short)
			contenido.save()
			template = loader.get_template("acorta/formulario_url.html")
			html = f"{template}"\
				   f"<div>{obtener_contenido()}</div>"
			return HttpResponse(html)
	else:
		template = loader.get_template("acorta/formulario_url.html")
		html = f"{template}" \
			   f"<div>{obtener_contenido()}</div>"
		return HttpResponse(html)


@csrf_exempt
def get_resource(request, url):
	print(url)
	try:
		url = f"localhost://8000/{url}"
		contenido = Contenido.objects.get(short=url)
		return HttpResponseRedirect(contenido.url)

	except Contenido.DoesNotExist:
		return HttpResponse("ERROR 404")

