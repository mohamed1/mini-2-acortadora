from django.db import models

# Create your models here.
class Contenido(models.Model):
    url = models.URLField()
    short = models.URLField()
